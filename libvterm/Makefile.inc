override CFLAGS +=-Wall -Iinclude -std=c99 -Wpedantic

ifeq ($(shell uname),SunOS)
  override CFLAGS +=-D__EXTENSIONS__ -D_XPG6 -D__XOPEN_OR_POSIX
endif

ifeq ($(DEBUG),1)
  override CFLAGS +=-ggdb -DDEBUG
endif

ifeq ($(PROFILE),1)
  override CFLAGS +=-pg
  override LDFLAGS+=-pg
endif

CFILES=$(sort $(wildcard src/*.c))
HFILES=$(sort $(wildcard include/*.h))
OBJECTS=$(CFILES:.c=.o)
LIBRARY=libvterm.a

AR?=ar
RANLIB?=ranlib

TBLFILES=$(sort $(wildcard src/encoding/*.tbl))
INCFILES=$(TBLFILES:.tbl=.inc)

HFILES_INT=$(sort $(wildcard src/*.h)) $(HFILES)

VERSION_MAJOR=0
VERSION_MINOR=0

VERSION_CURRENT=0
VERSION_REVISION=0
VERSION_AGE=0

VERSION=0

PREFIX=/usr/local
BINDIR=$(PREFIX)/bin
LIBDIR=$(PREFIX)/lib
INCDIR=$(PREFIX)/include
MANDIR=$(PREFIX)/share/man
MAN3DIR=$(MANDIR)/man3

all: $(LIBRARY)

$(LIBRARY): $(OBJECTS)
	@echo AR $@
	$(AR) rvs $@ $^
	$(RANLIB) $@

src/%.o: src/%.c $(HFILES_INT)
	@echo CC $<
	$(CC) $(CFLAGS) -o $@ -c $<

src/encoding/%.inc: src/encoding/%.tbl
	@echo TBL $<
	@perl -CSD tbl2inc_c.pl $< >$@

src/encoding.o: $(INCFILES)

.PHONY: clean
clean:
	rm -f $(OBJECTS) $(INCFILES)
	rm -f t/harness.lo t/harness
	rm -f $(LIBRARY) $(BINFILES)

