CFLAGS  +=-Wall -Ilibvterm/include -std=c11 -DGDK_DISABLE_DEPRECATED -DGTK_DISABLE_DEPRECATED -DGSEAL_ENABLE -DGTK_DISABLE_SINGLE_INCLUDES -g
LDFLAGS += -lutil -Llibvterm -lvterm -lm

ifeq ($(DEBUG),1)
  CFLAGS +=-ggdb -DDEBUG
endif

ifeq ($(PROFILE),1)
  CFLAGS +=-pg
  LDFLAGS+=-pg
endif

CFLAGS  +=$(shell pkg-config --cflags gl)
LDFLAGS +=$(shell pkg-config --libs   gl)

CFLAGS  +=$(shell pkg-config --cflags gtk+-3.0)
LDFLAGS +=$(shell pkg-config --libs   gtk+-3.0)

CFLAGS  +=$(shell pkg-config --cflags cairo)
LDFLAGS +=$(shell pkg-config --libs   cairo)

PREFIX=/usr/local
BINDIR=$(PREFIX)/bin
SHAREDIR=$(PREFIX)/share

CFLAGS+=-DPANGOTERM_SHAREDIR="\"$(SHAREDIR)\""

HFILES=$(wildcard *.h)
CFILES=$(wildcard *.c)
OBJECTS=$(CFILES:.c=.o)

all: pangoterm

pangoterm: $(OBJECTS) libvterm/libvterm.a
	@echo LINK $@
	$(CC) $(CFLAGS) -o $@ $^ $(LDFLAGS)

%.o: %.c $(HFILES)
	@echo CC $<
	$(CC) $(CFLAGS) -o $@ -c $<

nanovg.o: nanovg/src/nanovg.c
	@echo CC $<
	$(CC) $(CFLAGS) -o $@ -c $<

.PHONY: libvterm/libvterm.a
libvterm/libvterm.a:
	$(MAKE) -C libvterm -f Makefile.inc

.PHONY: clean
clean:
	rm -f $(OBJECTS)
	rm -f pangoterm
	$(MAKE) -C libvterm -f Makefile.inc clean

