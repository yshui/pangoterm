#pragma once

#include "vterm.h"

#include <gtk/gtk.h>

typedef struct PangoTerm PangoTerm;

PangoTerm *pangoterm_new(int rows, int cols);
void pangoterm_free(PangoTerm *pt);

unsigned long pangoterm_get_xid(PangoTerm *pt);

void pangoterm_set_default_colors(PangoTerm *pt, GdkRGBA *fg_col, GdkRGBA *bg_col);
void pangoterm_set_fontsize(PangoTerm *pt, double size);
void pangoterm_set_fonts(PangoTerm *pt, char *font, char *font_italic, char **alt_fonts); // ptr not value

void pangoterm_set_title(PangoTerm *pt, const char *title);

void pangoterm_start(PangoTerm *pt);

void pangoterm_begin_update(PangoTerm *pt);
void pangoterm_push_bytes(PangoTerm *pt, const char *bytes, size_t len);
void pangoterm_end_update(PangoTerm *pt);

typedef size_t PangoTermWriteFn(const char *bytes, size_t len, void *user);
void pangoterm_set_write_fn(PangoTerm *pt, PangoTermWriteFn *fn, void *user);

typedef void PangoTermResizedFn(int rows, int cols, void *user);
void pangoterm_set_resized_fn(PangoTerm *pt, PangoTermResizedFn *fn, void *user);
